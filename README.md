# pandemics-images

Pandemics preprocessor to extract and reformat images.

## Usage

In your pandemics recipe, specify in your list of preprocessing hooks:

```json
{
  "preprocessing": [
    "pandemics-images"
  ]
}
```

## Options

- `--format=<ext>`: extension of the generated images (jpg|png|bmp|tiff|gif). Default = tiff.
- `--target='folderName'`: path to the folder where to store the new figures. If used from the command line, this is relative to the working directory. As a pandemic hook, this will be relative to the target folder. Default = 'figures'.
- `--rename`: if set, figures will be renamed 'Figure_1', 'Figure_2', etc.
- `--resolution=<DPI>`: resolution of the generated images.

## Path to files

As a pandemics preprocessor, this plugin writes on stdout a copy of the input where the path to the original images has been replaced by the path to the newly generated ones. Paths are relative to the path of the source document.
