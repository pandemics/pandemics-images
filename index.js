#!/usr/bin/env node

const path = require('path');
const Jimp = require('jimp');
const getStdin = require('get-stdin');
const fs = require('fs-extra');

// make imageMagick accesible
const env = require('./lib/gm-bin.js')(process.env);
const minimist = require('minimist')

const opts = minimist(
  process.argv.slice(2),
  {default: {
    target: 'figures',
    format: 'tiff',
    rename: false,
    resolution: 300
  }}
);

// target directory
const sourceDir = env.PANDOC_SOURCE_PATH || process.cwd();
const targetDir = path.join(
  env.PANDOC_TARGET_PATH || process.cwd(),
  opts.target
);
fs.ensureDirSync(targetDir);

// read from stdin
var content = fs.readFileSync(0, 'utf8');

// find images
const myRegexp = /!\[.*?\]\((.+?)\)/g;
var imageList = [];
imgCpt = 1;
while (match = myRegexp.exec(content)) {

  // store markup and token
  image = {
    markup: match[0],
    source: match[1]
  }

  // extract filename
  let imgName
  if (opts.rename) {
    image.name = `Figure_${imgCpt}`;
    imgCpt ++;
  } else {
    image.name = path.basename(
      image.source,
      path.extname(image.source)
    );
  }

  // full path
  image.sourceFile = path.join(
    sourceDir,
    image.source
  );
  image.targetFile = path.join(
    targetDir,
    image.name + '.' + opts.format
  );

  // relative path
  image.relPath = path.relative(sourceDir, image.targetFile);

  // store
  imageList.push(image);
}

// generate images
imageList.forEach((image) => {
  Jimp.read(image.sourceFile)
  .then(img => {
    img
    .write(image.targetFile);
  })
  .catch(err => {
    // TODO: delete prevously generated images
    throw err;
  });
});

// update content
imageList.forEach((image) => {
  // prepare new markup
  content = content.replace(
    image.markup,
    image.markup.replace(image.source, image.relPath)
  )
});

// write to the pipe
process.stdout.write(content);
