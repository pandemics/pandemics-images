var path = require('path');

function loadGM(env) {

  // look for correct path to graphics magick
  let gmPath = path.join(__dirname, '..', 'vendor');
  if (process.platform === 'darwin') {
    gmPath = path.join(gmPath,'osx');
  } else if (process.platform === 'linux') {
    gmPath = path.join(gmPath,'linux');
  /*
  }  else if (process.platform === 'win32') {
    if (process.arch === 'x64') {
    } else {
    }
  */
  } else {
    throw new Error('Unsupported platform:', process.platform, process.arch);
  }

  //make gm accessible
  env.PATH += path.delimiter + path.join(gmPath,'bin');
  env.DYLD_LIBRARY_PATH = path.join(gmPath,'lib');

  return env;
}

module.exports = loadGM;
